import numpy as np
import cv2
import threading
import screeninfo

class cv2_windows:
    
    def __init__(self, keep_ratio=True):
        self.th = None
        self.keeprunning = False
        self.win_d = {}
        self.fullscreen = False
        self.keep_ratio = keep_ratio

        mon = screeninfo.get_monitors()[0]
        self.mon_size = mon.width, mon.height
        return None
    

    def open_window(self,
                    win_shape=(600,600),
                    win_name='Prediction'):
        
        if self.th is None:
            self.th = threading.Thread( target=self._win_update )
            self.keeprunning = True
            self.lock = threading.Lock()
            self.th.start()
        
        win_img = 128*np.ones( win_shape, dtype=np.uint8 )
        with self.lock:
            self.win_d[win_name] = [win_shape, win_img]

        return None


    def close_windows(self):
        self.keeprunning = False
        return None


    def _win_update(self):

        while self.keeprunning:
            with self.lock:
                for win_name, (win_shape, win_img) in self.win_d.items():
                    cv2.imshow(win_name, win_img)
                
            wkey = cv2.waitKey(10) & 0xFF
            if wkey == ord('q'):
                break
            
            elif wkey == ord('f'):
                self.fullscreen = not self.fullscreen
                cv2.destroyAllWindows()
                
                if self.fullscreen:
                    for win_name, (win_shape, win_img) in self.win_d.items():
                        cv2.namedWindow(win_name, cv2.WND_PROP_FULLSCREEN)
                        cv2.setWindowProperty(win_name, cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)

                with self.lock:
                    self._resize_all_imgs()
                    
        self.keeprunning = False
        cv2.destroyAllWindows()
        self.th = None

        return None

    def _resize_all_imgs(self):
        for win_name, (win_shape, win_img) in self.win_d.items():
            if self.fullscreen:
                if self.keep_ratio:
                    m_w, m_h = self.mon_size
                    i_h, i_w = win_shape
                    zoom = min(m_w/i_w, m_h/i_h)

                    new_size = (min(m_w, int(zoom*i_w)), min(m_h, int(zoom*i_h)))
                    win_img = cv2.resize(win_img, new_size)

##                    print('self.mon_size:', self.mon_size)
##                    print('win_img.shape:', win_img.shape)
##                    print('new_size:', new_size)

                    yoff = round( (m_h-new_size[1])/2 )
                    xoff = round( (m_w-new_size[0])/2 )

                    img_black = np.zeros( (self.mon_size[1], self.mon_size[0], 3), dtype=np.uint8)

##                    print('img_black.shape:', img_black.shape)
                    img_black[yoff:yoff+new_size[1], xoff:xoff+new_size[0]] = win_img

                    win_img = img_black
                    
                else:
                    win_img = cv2.resize(win_img, self.mon_size )
            else:
                win_img = cv2.resize(win_img, tuple(win_shape[::-1]) )

            self.win_d[win_name][1] = win_img
        return None

    def update_img(self, img, win_name=None):
        """ the image needs to be in RGB mode """
        
        with self.lock:
            if win_name is None and len(self.win_d.keys()) == 1:
                win_name = list(self.win_d.keys())[0]
                
            elif win_name is None and len(self.win_d.keys()) > 1:
                raise Exception(' - ERROR, update_img: you need to specify a win_name if there are more than one windows opened.')
        
            if win_name not in self.win_d.keys():
                raise Exception(' - ERROR, update_img: bad window name.')

        if len(img.shape) == 2:
            img = np.repeat(img[...,np.newaxis], 3, axis=-1)

        if img.dtype == np.float:
            img = (img * 255).astype(np.uint8)
            
        img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)
        
        with self.lock:
            self.win_d[win_name][1] = img
            self._resize_all_imgs()
        
        return None


if __name__ == '__main__':
    win = cv2_windows()
    win.open_window(win_shape=(400,700), win_name='Prediction')
##    win.open_window(win_shape=(200,600), win_name='Prediction2')

    img = cv2.imread('../samples/people2.jpg')[:,:,::-1]
    win.update_img(img)
