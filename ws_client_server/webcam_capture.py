import os, sys
import io
import time
import numpy as np
import cv2
import pickle
import threading

from PIL import Image

from cv2_windows import cv2_windows
from websocket_client import ws_client


def draw_boxes(image, boxes, labels=None, probs=None, do_copy=False, px_thickness=1, color=(36,255,12)):

    if image.dtype == np.float:
        color = [c/255.0 for c in color]
        
    if do_copy:
        image = np.copy(image)
        
    for i_box, (t, l, b, r) in enumerate(boxes):
##        print('Box', i_box , ':', (t, l, b, r))
        image = cv2.rectangle(image, (l, t), (r, b), color, px_thickness)

        text = ''
        if labels is not None:
            text += str( labels[i_box] )

        if probs is not None:
            text += '[{:0.02f}]'.format( probs[i_box] )

        if text != '':
            t_text = t-10 if t - 10 > 10 else t + 10
            cv2.putText(image, text, (l, t_text), cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, px_thickness, cv2.LINE_AA)
        
    return image


def put_boxes(img, pred_d):
    f_female = (pred_d['genders_class_v'] == 1)
    
    img_pred = draw_boxes(img,
                          boxes=pred_d['faces_boxes_v'][f_female],
                          labels=pred_d['genders_v'][f_female],
                          probs=pred_d['gender_scores_v'][f_female],
                          px_thickness=1,
                          color=(255,0,255),
                          do_copy=True)

    img_pred = draw_boxes(img_pred,
                          boxes=pred_d['faces_boxes_v'][~f_female],
                          labels=pred_d['genders_v'][~f_female],
                          probs=pred_d['gender_scores_v'][~f_female],
                          px_thickness=1,
                          color=(0,255,255),
                          do_copy=False)

    return img_pred

    
def gender_recognition(self):
    raw_frame  = self.capture_cam() # update captured size

    if self.window is None:
        self.windows = cv2_windows()
        self.windows.open_window(win_shape=self.captured_size, win_name='Camera')
    else:
        self.windows = self.window

    self.keep_running = True
    
    while self.keep_running:
        if self.cap.isOpened():
            last_cap_time = time.time()
            raw_frame  = self.capture_cam()
            img_bytes = img2bytes(raw_frame)
            
            if self.client.connected:
                if self.n_imgs_sent < self.n_imgs_proc_max:
                    
                    self.client.send(img_bytes)
                    self.n_imgs_sent += 1

                    time_to_wait = max(0.0, 1 / self.target_fps - time.time() + last_cap_time)
                    if time_to_wait > 0:
                        time.sleep(time_to_wait)
                else:
                    print(' - WARNING, capture_forever: Oversent imgs, you can try lower the target fps ...', file=sys.stderr)
                    time.sleep(0.1)
            else:
                print(' - WARNING, the client is not connected ...', file=sys.stderr)
                time.sleep(1.0)

    self.windows.close_windows()
    return None


def img2bytes(img, format='JPEG'):
    if img.dtype == np.float:
        img = np.clip(img*255, 0 ,255).astype(np.uint8)
        
    image      = Image.fromarray(img)
    imgByteArr = io.BytesIO()
    image.save(imgByteArr, format=format)
    imgByteArr = imgByteArr.getvalue()
    return imgByteArr

        
def on_message(ws, message):
    detector = ws.detector
    
##    print('on_message:', message)

    if type(message) in [bytes, bytearray]:
        detector.n_imgs_sent -= 1
        
        pred_d = pickle.loads(message)

##        print('pred_d:', pred_d)
        
        pred_d['time'] = time.time()
        
        detector.preds_v.pop(0)
        detector.preds_v.append(pred_d)

        if detector.last_frame is not None:
            img = detector.last_frame
            img = put_boxes(img, pred_d)
            detector.windows.update_img(img=img, win_name='Camera')
            
    else:
        print('on_message: type={} msg={}'.format(type(message), message))
        
    return None


class GenderDetectorClient():
    def __init__(self,
                 cam_index=0,
                 target_fps=10,
                 host='localhost',
                 port=8080,
                 password='yolo_gender',
                 window=None):
        
        """ If cam_index will read the file instead of the webcam."""

        
        self.cam_index = int(cam_index)
        self.cap = cv2.VideoCapture(self.cam_index)
        self.target_fps = target_fps

        self.host = host
        self.port = port
        self.password = password

        self.genders_detected = []
        self.keep_running = False

        self.n_imgs_proc_max = 3
        self.n_imgs_sent = 0
        

        self.client = ws_client(host=self.host,
                                port=self.port,
                                on_message_function=on_message,
                                password=self.password)

        self.last_frame = None
        self.preds_v = [None] * 50
        self.window = window
        return None


    def connect(self):
        if not self.client.connected:
            self.client.start()
            self.client.ws.detector = self
            
        return None

    def start_recognition(self):
        self.connect()
        self.th = threading.Thread(target=gender_recognition, args=(self,))
        self.th.start()

        return None

    def stop_recognition(self):
        self.keep_running = False
        return None

    def capture_cam(self):
        if self.cap is not None and self.cap.isOpened():
            ret, frame = self.cap.read()
            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)

            self.last_frame = frame
            self.captured_size = (frame.shape[0], frame.shape[1])

        return frame
    

    def test_camera(self):

        raw_frame  = self.capture_cam()

        if self.window is None:
            self.windows = cv2_windows()
            self.windows.open_window(win_shape=(raw_frame.shape[0], raw_frame.shape[1]), win_name='Camera')
        else:
            self.windows = self.window
        
        

        while True:
            last_frame = self.last_frame
            raw_frame  = self.capture_cam()
            
##            diff = np.mean( np.square(raw_frame - last_frame) )
##            print('frame_diff:', diff)

            self.windows.update_img(img=raw_frame, win_name='Camera')
            
            time.sleep(1/self.target_fps)

        return None

    def close(self):
        self.windows.close_windows()
        if self.cap is not None:
            self.cap.release()

        if self.client.connected:
            self.client.close()
        
        return None


    def get_gender(self, dt=10):
        actual_time = time.time()

        confidence_m = []
        for pred_d in self.preds_v:
            if pred_d is not None and actual_time - pred_d['time'] < dt:                
                if len(pred_d['gender_scores_v']) > 0:
                    idx = np.argmax(pred_d['gender_scores_v'] * pred_d['faces_scores_v'])
                    
                    if pred_d['genders_class_v'][idx] == 1:
                        confidence_m.append([0, pred_d['gender_scores_v'][idx]])
                    else:
                        confidence_m.append([pred_d['gender_scores_v'][idx], 0])

        if len(confidence_m) > 0:
            confidence_m = np.array(confidence_m)
            p_v = confidence_m.mean(axis=0)
            
            return 'Male' if p_v[0] > p_v[1] else 'Female', p_v, confidence_m
        else:
            return None
        
            
if __name__ == '__main__':


    detector = GenderDetectorClient()
##    detector.start_recognition()    
    detector.test_camera()




    
