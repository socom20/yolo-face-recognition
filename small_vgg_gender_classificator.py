import random
import os, sys
import glob
import pickle
from collections import namedtuple


import numpy as np
import matplotlib.pyplot as plt
import cv2


import tensorflow.keras as keras

from yoloface.YOLO import YOLO, YOLO_args, extract_faces, draw_boxes, letterbox_image_cv2
from model import SmallerVGGNet

##from keras.models import load_model

class GenderClassificator:
    
    def __init__(self,
                 gender_model_path='./models/model_e06_b000_l=0.072_a=0.978.kmodel',
                 yolo_model='./model-weights/YOLO_Face.h5',
                 yolo_anchors='./yoloface/cfg/yolo_anchors.txt',
                 yolo_classes='./yoloface/cfg/face_classes.txt',
                 model_input_shape=(92,92),
                 max_num_of_faces=3,
                 verbose=True):

        self.yolo_model   = yolo_model
        self.yolo_anchors = yolo_anchors
        self.yolo_classes = yolo_classes
        self.gender_model_input_shape = model_input_shape
        self.max_num_of_faces = max_num_of_faces
        self.verbose = verbose
        
        self._load_yolo()

        if gender_model_path is None:
            self.gender_model = None
        else:
            self.gender_model = self.load_gender_model(gender_model_path)
            

        self.IDG = keras.preprocessing.image.ImageDataGenerator(featurewise_center=False,
                                                                samplewise_center=False,
                                                                featurewise_std_normalization=False,
                                                                samplewise_std_normalization=False,
                                                                zca_whitening=False,
                                                                zca_epsilon=1e-06,
                                                                rotation_range=10,
                                                                width_shift_range=5.0,
                                                                height_shift_range=5.0,
                                                                brightness_range=None,
                                                                shear_range=0.0,
                                                                zoom_range=0.0,
                                                                channel_shift_range=0.1,
                                                                fill_mode='constant',
                                                                cval=0.0,
                                                                horizontal_flip=True,
                                                                vertical_flip=False,
                                                                rescale=None,
                                                                preprocessing_function=None,
                                                                data_format='channels_last',
                                                                validation_split=0.0,
                                                                dtype='float32')
        return None


    def _load_yolo(self):
        self.yolo_args = YOLO_args(model=self.yolo_model,
                                   anchors=self.yolo_anchors,
                                   classes=self.yolo_classes,
                                   extend_boxes_prop=1.3,
                                   max_boxes=self.max_num_of_faces,
                                   verbose=False)

        self.yolo = YOLO(self.yolo_args)

        if self.verbose:
            print(' - YOLO face modelo loaded:', self.yolo_model)
        return None


    def face_detect(self, image):
        boxes_v, scores_v = self.yolo.detect_image((255*image))
        return boxes_v, scores_v


    def extract_faces(self, image, boxes_v):
        faces_v = extract_faces(image, boxes_v)
        return faces_v

    
    def get_input_faces(self, image):
        
        boxes_v, scores_v = self.face_detect(image)
        faces_v = self.extract_faces(image, boxes_v)
        
        for i_f in range(len(faces_v)):
            face = faces_v[i_f]

            face = letterbox_image_cv2(face,
                                       shape=self.gender_model_input_shape,
                                       color=(0,0,0))
            
            p_max = face.max()
            p_min = face.min()
            
            face = (face - p_min) / (p_max - p_min)

            faces_v[i_f] = face


        return np.array(faces_v), scores_v, boxes_v
        
        
    def make_datasets(self, ds_paths_v=[], validation_split=0.2, seed=0, make_cache=False, cache_folder='./datasets'):
        """ Read all files containded un ds_paths_v,
            The ds_path must contained subfolders "man" and "wommen", with the respective images files.

            validation_split: float proportion for the validation split. for each ds_path this proportion is used.
            return (ds_trn, ds_val) : datasets containing the sample paths.
        """
            
                
        ds_trn = {'man':[], 'woman':[]}
        ds_val = {'man':[], 'woman':[]}
            
        if make_cache and os.path.exists(cache_folder+'/ds_trn_man.pickle') and os.path.exists(cache_folder+'/ds_trn_woman.pickle'):
                                                               
            for i_ds, ds in enumerate([ds_trn, ds_val]):
                for gender in ['man', 'woman']:
                    file_name = cache_folder+'/{}_{}.pickle'.format(['ds_trn', 'ds_val'][i_ds], gender)

                    print(' - Reading {} ...'.format(file_name), end=' ')
                    with open(file_name, 'rb') as f:
                        ds['{}_img_v'.format(gender)] = pickle.load(f)

                    print('OK!')

        else:
            if seed is not None:
                random.seed(seed)
            
            for ds_path in ds_paths_v:
                if self.verbose:
                    print(' - make_datasets, reading:', ds_path)
                    
                man_samples_v  = glob.glob(ds_path+'/man/*')
                woman_samples_v = glob.glob(ds_path+'/woman/*')

                n_man   = len(man_samples_v)
                n_woman = len(woman_samples_v)

                if self.verbose:
                    print(' - Found: n_samples_man = {}   n_samples_woman = {} '.format(n_man, n_woman))
                    
                
                man_samples_v.sort()
                woman_samples_v.sort()
                
                random.shuffle( man_samples_v )
                random.shuffle( woman_samples_v )

                n_trn_man = int(n_man * (1-validation_split))
                ds_trn['man'] += man_samples_v[:n_trn_man]
                ds_val['man'] += man_samples_v[n_trn_man:]

        
                n_trn_woman = int(n_woman * (1-validation_split))
                ds_trn['woman'] += woman_samples_v[:n_trn_woman]
                ds_val['woman'] += woman_samples_v[n_trn_woman:]

            if self.verbose:
                print(' Total of samples readed:')
                print(' - ds_trn:  n_man = {}   n_woman = {}'.format(len(ds_trn['man']), len(ds_trn['woman'])))
                print(' - ds_val:  n_man = {}   n_woman = {}'.format(len(ds_val['man']), len(ds_val['woman'])))

            if make_cache:
                for i_ds, ds in enumerate([ds_trn, ds_val]):
                    for gender in ['man', 'woman']:
                        key = '{}_img_v'.format(gender)
                        ds[key] = []
                        
                        for i_s, img_path in enumerate(ds[gender]):
                            if i_s % 50 == 0:
                                print("Makeing cache for {}['{}']: {:0.02f}% complete".format(['ds_trn', 'ds_val'][i_ds], gender, i_s/len(ds[gender])*100))
                                
                            ds[key].append( self.read_image(img_path) )

                        file_name = cache_folder+'/{}_{}.pickle'.format(['ds_trn', 'ds_val'][i_ds], gender)
                        print(' Writing dataset cache {}...'.format(file_name), end=' ')
                        with open(file_name, 'wb') as f:
                            pickle.dump( ds[key], f)

                        print('OK')
                    
        return ds_trn, ds_val


    def augment_image(self, image):
        img_augmented = self.IDG.random_transform(image)
        return img_augmented


    def read_image(self, image_path):
        """ Reads an image from a file.

            return np.ndarray with thre format (H,W,Channel)  Channel=RGB
            return None if an error occurred
            """
            
        try:
            img = cv2.imread(image_path)
            if img is None:
                print(' - ERROR, read_image: file not found:', image_path, file=sys.stderr)
                return None
            
        except:
            print(' - ERROR, read_image: error while reading:', image_path, file=sys.stderr)
            return None
        
        return img[:,:,::-1]/255.0
    

    def read_sample(self, image, do_data_augmentation=True):
        """ Prepares an image for training. Returns the faces detected in it, does an augmentation if required.
            Returns a face image if found, else returns None
        """
        
        if do_data_augmentation:
            image = self.augment_image(image)

        faces_v, scores_v, boxes_v = self.get_input_faces(image)

        if len(scores_v) == 1:
            face = faces_v[0]
            
        elif len(scores_v) > 1:
            face = faces_v[np.argmax(scores_v)]
            
        else:
            face = None
            
        return face

        
        
    def get_batch_generator(self, ds, batch_size=32, n_epochs=99999, do_data_augmentation=True):

        assert batch_size % 2 == 0, ' - ERROR, get_batch_generator, batch_size must be even.'

        n_samples          = (len(ds['man']) + len(ds['woman']))
        n_batchs_per_epoch = n_samples // batch_size
        
        def generator():
            use_cache_man   = 'man_img_v' in ds.keys()
            use_cache_woman = 'woman_img_v' in ds.keys()

            if use_cache_man:
                n_man = len(ds['man_img_v'])
            else:
                n_man = len(ds['man'])


            if use_cache_woman:
                n_woman = len(ds['woman_img_v'])
            else:
                n_woman = len(ds['woman'])

            
            for i_batch in range(n_epochs * n_batchs_per_epoch):
                x_v = []
                y_v = []

                
                while len(x_v) < batch_size//2:
                    i_sample = random.randint(0, n_man-1)
                    
                    if use_cache_man:
                        image = ds['man_img_v'][i_sample]
                    else:
                        image = self.read_image(ds['man'][i_sample])

                    if image is None:
                        continue
                    
                    face = self.read_sample(image,
                                            do_data_augmentation=do_data_augmentation)

                    if face is None:
                        continue

                    x_v.append(face)
                    y_v.append(np.array([1.0, 0.0]))


                while len(x_v) < batch_size:
                    i_sample = random.randint(0, n_woman-1)
                    
                    if use_cache_woman:
                        image = ds['woman_img_v'][i_sample]
                    else:
                        image = self.read_image(ds['woman'][i_sample])

                    if image is None:
                        continue
                    
                    face = self.read_sample(image,
                                            do_data_augmentation=do_data_augmentation)

                    if face is None:
                        continue

                    x_v.append(face)
                    y_v.append(np.array([0.0, 1.0]))    

                yield np.array(x_v), np.array(y_v)


        return generator(), n_batchs_per_epoch


    def build_new_gender_model(self):
        self.gender_model = SmallerVGGNet.build(self.gender_model_input_shape[0], self.gender_model_input_shape[1], 3, 2)
        return None

    def load_gender_model(self, gender_model_path='./models/model_e61_b000_l=0.105_a=0.978.kmodel'):
        self.gender_model = keras.models.load_model(gender_model_path)
        if self.verbose:
            print(' - Gender Model Loaded:', gender_model_path)
        return self.gender_model
    
    
    def train_model(self, ds_trn, ds_val, batch_size=32, n_epochs=50, n_val_batchs=10, learning_rate=1e-3, models_path = './models', verbose=False):        
        if not os.path.exists(models_path):
            print(' - Creating folder:', models_path)
            os.mkdir(models_path)
            
            
        if self.gender_model is None:
            self.build_new_gender_model()

        self.gen_trn, self.n_batchs_per_epoch_trn = self.get_batch_generator(ds_trn, batch_size=batch_size, n_epochs=9999, do_data_augmentation=True)
        self.gen_val, self.n_batchs_per_epoch_val = self.get_batch_generator(ds_val, batch_size=n_val_batchs*batch_size, n_epochs=9999, do_data_augmentation=False)

        opt = keras.optimizers.Adam(learning_rate,
                                    decay=learning_rate/50)
        
        self.gender_model.compile(opt, loss='binary_crossentropy', metrics=['binary_accuracy'])
        print(' - Model compiled ...')

        self.n_batchs_per_epoch_trn = self.n_batchs_per_epoch_trn

        acc_val_max = 0.0
        for i_batch_, (x_v, y_v) in enumerate(self.gen_trn):
            i_epoch = i_batch_ // self.n_batchs_per_epoch_trn
            i_batch = i_batch_ % self.n_batchs_per_epoch_trn
            
            
            if i_batch == 0:
                if i_batch_ == 0:
                    print('Starting training')
                else:
                    print('Epoch complete!!!')
                    
                x_val_v, y_val_v = next(self.gen_val)
                loss_val, acc_val = self.gender_model.evaluate(x_val_v, y_val_v, batch_size, verbose=False)
                print(' - VALIDATION summary:    val_loss={:0.03f}   val_acc={:0.03f}'.format(loss_val, acc_val))

                if acc_val > acc_val_max - 0.05:
                    model_file_name = '{}/model_e{:02d}_b{:03d}_l={:0.03f}_a={:0.03f}.kmodel'.format(models_path,
                                                                                                    i_epoch,
                                                                                                    i_batch,
                                                                                                    loss_val,
                                                                                                    acc_val)
                    self.gender_model.save(model_file_name)
                    print(' Model saved:', model_file_name)
                    
                    acc_val_max = max(acc_val_max, acc_val)


            loss, acc = self.gender_model.train_on_batch(x_v, y_v)
            print(' - Epoch:{:2d}  batch:{:3d} of {:3d}   batch_loss={:0.03f}   batch_acc={:0.03f}'.format(i_epoch, i_batch, self.n_batchs_per_epoch_trn, loss, acc))    
            
        return None


    def predict_face_and_gender(self, image):
        """ Predict faces positions and assign a gender for each one.
            Also reports the scores for the prediction.
            """

        genders_v = ['male','female']

        
        faces_v, faces_scores_v, faces_boxes_v = self.get_input_faces(image)

        if len(faces_v) > 0:
            pred_gender_v = self.gender_model.predict(faces_v)
            
            genders_v  = np.array([genders_v[i_g] for i_g in pred_gender_v.argmax(axis=-1)])
            gender_scores_v = pred_gender_v.max(axis=-1)
            genders_class_v = pred_gender_v.argmax(axis=-1)
            
        else:
            genders_v       = np.array([])
            gender_scores_v = np.array([])
            genders_class_v = np.array([])


        ret_tuple = namedtuple('Prediction_Tuple', 'faces_boxes_v faces_scores_v genders_v gender_scores_v genders_class_v')
        
        return ret_tuple(faces_boxes_v, faces_scores_v, genders_v, gender_scores_v, genders_class_v)
        
        
    def put_boxes(self, img, pred):
        f_female = (pred.genders_class_v == 1)
        
        img_pred = draw_boxes(img,
                              boxes=pred.faces_boxes_v[f_female],
                              labels=pred.genders_v[f_female],
                              probs=pred.gender_scores_v[f_female],
                              px_thickness=1,
                              color=(255,0,255),
                              do_copy=True)

        img_pred = draw_boxes(img_pred,
                              boxes=pred.faces_boxes_v[~f_female],
                              labels=pred.genders_v[~f_female],
                              probs=pred.gender_scores_v[~f_female],
                              px_thickness=1,
                              color= 	(0,255,255),
                              do_copy=False)

        return img_pred


    

if __name__ == '__main__':
##    import tensorflow as tf
##    config = tf.ConfigProto()
##    config.gpu_options.allow_growth = True
##    sess = tf.Session(config=config)


    

    # Preproc:
##    img = gc.read_image('./samples/people1.jpg')
##    face_v, scores_v, boxes_v = gc.get_input_faces(img)
##    for f in face_v:
##        plt.imshow(f)
##        plt.show()

    if 1:
        gc = GenderClassificator(
##            gender_model_path='./models/model_e10_b000_l=0.054_a=0.982.kmodel',
            gender_model_path='./models/model_e06_b000_l=0.072_a=0.978.kmodel',
            max_num_of_faces=150)


    else:
        # Train Model
        gc = GenderClassificator(
            gender_model_path=None,
            max_num_of_faces=150)

        
        ds_trn, ds_val = gc.make_datasets(
            ds_paths_v=[
                './datasets/a1_ds',
                './datasets/a6_ds',
                './datasets/GENIK',
                './datasets/caleb_ds',
                './datasets/mask_ds',
                './datasets/parts_ds'
                ],
            make_cache=False,
            )

        gc.train_model(ds_trn, ds_val, batch_size=128, n_epochs=9999, n_val_batchs=10, verbose=True)


    
##    for file_name in glob.glob('./samples/*.jpg'):
    if 1:
        # Image Prediction
        
        file_name = './samples/people3.jpg'
        
        img = gc.read_image(file_name)
        
        pred = gc.predict_face_and_gender(img)
        img_pred = gc.put_boxes(img, pred)

        
        plt.imshow(img_pred)
        plt.show()


        from PIL import Image
        img_pil = Image.fromarray( (img_pred*255.0).astype(np.uint8) )
        
        if not os.path.exists('./outputs'):
            os.mkdir('./outputs')
        
##        img_pil.save(os.path.join('./outputs', os.path.split(file_name)[-1]) )
                         
        
    if 0:
        # Video Prediction
        video_path  = './samples/bsas_completo.mp4'
        output_path = './outputs/output3.mp4'

        cap = cv2.VideoCapture(video_path)
        cap.set(cv2.CAP_PROP_POS_MSEC, 10000 )

        video_size = (int(cap.get(cv2.CAP_PROP_FRAME_WIDTH)), int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT)))
        fourcc = cv2.VideoWriter_fourcc(*'XVID')
        out = cv2.VideoWriter(output_path, fourcc, 30.0, video_size)
        
        n_frame = 0
        while(cap.isOpened()):
            ret, frame = cap.read()
            n_frame += 1

            if (n_frame % 2) != 0:
                continue
            
            if frame is None:
                break
            
            
            img = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB) / 255

            pred = gc.predict_face_and_gender(img)
            img_pred = gc.put_boxes(img, pred)

            
            frame = cv2.cvtColor((img_pred*255).astype(np.uint8), cv2.COLOR_RGB2BGR) 

            frame = cv2.resize(frame, video_size)
            
            out.write(frame)
            cv2.imshow('Prediction', frame)
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break


        cv2.destroyAllWindows()
        cap.release()
        out.release()

        
    if 0:
        # Cam Prediction
        cap = cv2.VideoCapture(0)
        
        while 1:
            ret, frame = cap.read()
            
            
            if ret:
                img = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB) / 255
                
                pred = gc.predict_face_and_gender(img)
                img_pred = gc.put_boxes(img, pred)
                frame = cv2.cvtColor((img_pred*255).astype(np.uint8), cv2.COLOR_RGB2BGR) 
                cv2.imshow('CamPrediction', frame)
                
            
            if not ret or cv2.waitKey(1) & 0xff == ord('q'):
                cv2.destroyAllWindows()
                cap.release()
                break
                


