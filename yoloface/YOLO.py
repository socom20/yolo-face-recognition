# *******************************************************************
#
# Author : Thanh Nguyen, 2018
# Email  : sthanhng@gmail.com
# Github : https://github.com/sthanhng
#
# Face detection using the YOLOv3 algorithm
#
# Description : yolo.py
# Contains methods of YOLO
#
# *******************************************************************

import os
import colorsys
import numpy as np

from yoloface.model import eval

from tensorflow.compat.v1.keras import backend as K
from tensorflow.compat.v1.keras.models import load_model
from timeit import default_timer as timer

import cv2


class YOLO_args():
    def __init__(self,
                 model='../model-weights/YOLO_Face.h5',
                 anchors='../yoloface/cfg/yolo_anchors.txt',
                 classes='../yoloface/cfg/face_classes.txt',
                 score=0.5,
                 iou=0.45,
                 img_size=[416,416],
                 max_boxes=20,
                 extend_boxes_prop=1.0,
                 verbose=True):

        """ This class defines all args needed to init the YOLO v3 model.
            model: str, path to model weights file
            anchors: str, path to anchor definitions
            classes: str, path to class definitions
            score: float, the score threshold
            iou: float, the iou threshold
            img-size: [int, int], input image size
            image: bool, image detection mode
            video: str, path to the video
            output: str, image/video output path
            """
        
        self.model             = model
        self.anchors           = anchors
        self.classes           = classes
        self.score             = score
        self.iou               = iou
        self.img_size          = img_size
        self.max_boxes         = max_boxes
        self.extend_boxes_prop = extend_boxes_prop
        self.verbose           = verbose
        
        return None

    
class YOLO(object):
    def __init__(self, args):
        self.args = args
        self.model_path = args.model
        self.classes_path = args.classes
        self.anchors_path = args.anchors
        self.extend_boxes_prop = args.extend_boxes_prop
        self.verbose=args.verbose
        self.model_image_size = args.img_size

        
        self.class_names = self._get_class()
        self.anchors = self._get_anchors()
        self.sess = K.get_session()
        self.boxes, self.scores, self.classes = self._generate()
        
        return None

    def _get_class(self):
        classes_path = os.path.expanduser(self.classes_path)
        with open(classes_path) as f:
            class_names = f.readlines()
        class_names = [c.strip() for c in class_names]
        return class_names

    def _get_anchors(self):
        anchors_path = os.path.expanduser(self.anchors_path)
        with open(anchors_path) as f:
            anchors = f.readline()
        anchors = [float(x) for x in anchors.split(',')]
        return np.array(anchors).reshape(-1, 2)

    def _generate(self):
        model_path = os.path.expanduser(self.model_path)
        assert model_path.endswith(
            '.h5'), 'Keras model or weights must be a .h5 file'

        # load model, or construct model and load weights
        num_anchors = len(self.anchors)
        num_classes = len(self.class_names)
        try:
            self.yolo_model = load_model(model_path, compile=False)
        except:
            # make sure model, anchors and classes match
            self.yolo_model.load_weights(self.model_path)
        else:
            assert self.yolo_model.layers[-1].output_shape[-1] == \
                   num_anchors / len(self.yolo_model.output) * (
                           num_classes + 5), \
                'Mismatch between model and given anchor and class sizes'

        if self.verbose:
            print('*** {} model, anchors, and classes loaded.'.format(model_path))

        # generate colors for drawing bounding boxes
        hsv_tuples = [(x / len(self.class_names), 1., 1.)
                      for x in range(len(self.class_names))]
        
        self.colors = list(map(lambda x: colorsys.hsv_to_rgb(*x), hsv_tuples))
        self.colors = list(map(lambda x: (int(x[0] * 255), int(x[1] * 255), int(x[2] * 255)), self.colors))

        # shuffle colors to decorrelate adjacent classes.
        np.random.seed(102)
        np.random.shuffle(self.colors)
        np.random.seed(None)

        # generate output tensor targets for filtered bounding boxes.
        self.input_image_shape = K.placeholder(shape=(2,))
        boxes, scores, classes = eval(self.yolo_model.output,
                                      self.anchors,
                                      len(self.class_names),
                                      self.input_image_shape,
                                      max_boxes=self.args.max_boxes,
                                      score_threshold=self.args.score,
                                      iou_threshold=self.args.iou
                                      )
        return boxes, scores, classes

    def detect_image(self, image):

        if self.verbose:
            start_time = timer()

            
        assert self.model_image_size != (None, None), ' - ERROR: model_image_size not especified.'
        assert self.model_image_size[0] % 32 == 0, 'Multiples of 32 required'
        assert self.model_image_size[1] % 32 == 0, 'Multiples of 32 required'

        boxed_image = letterbox_image_cv2(image, tuple(reversed(self.model_image_size)))
        image_data = boxed_image / 255.0


        if len(image_data.shape) == 3:
            # add batch dimension
            image_data = np.expand_dims(image_data, 0)

        assert len(image_data.shape) == 4, 'Image must hace 4 dims (Batch, H, W, Channel)'

        out_boxes, out_scores, out_classes = self.sess.run(
            [self.boxes, self.scores, self.classes],
            feed_dict={
                self.yolo_model.input: image_data,
                self.input_image_shape: image.shape[:2], #[image.size[1], image.size[0]],
                K.learning_phase(): 0
            })


        out_boxes_int = []
        for top, left, bottom, right in out_boxes:
            top    = max(0, np.floor(top + 0.5).astype('int32'))
            left   = max(0, np.floor(left + 0.5).astype('int32'))
            bottom = min(image.shape[0], np.floor(bottom + 0.5).astype('int32'))
            right  = min(image.shape[1], np.floor(right  + 0.5).astype('int32'))
            
            out_boxes_int.append( [top, left, bottom, right] )

        out_boxes_int = np.array(out_boxes_int, dtype='int32')
        out_boxes_int = self.extend_boxes(image.shape, out_boxes_int, extend_prop=self.extend_boxes_prop)

        
        if self.verbose:
            print('*** YOLO: Found {} face(s) for this image'.format(len(out_boxes_int)))

        if self.verbose:
            end_time = timer()
            print('*** YOLO: Processing time: {:.2f}ms'.format((end_time - start_time) * 1000))

        return out_boxes_int, out_scores


    def extend_boxes(self, image_shape, boxes, extend_prop=1.0):

        if extend_prop == 1.0:
            return boxes
        
        boxes_ext = []
        for (t, l, b, r) in boxes:
            
            
            prop = extend_prop
            
            h = b-t
            w = r-l
            
            x = (b+t) / 2
            y = (r+l) / 2

            t = max(0, int(x - 0.5*h*prop))
            l = max(0, int(y - 0.5*w*prop))

            b = min(image_shape[0], int(x + 0.5*h*prop))
            r = min(image_shape[1], int(y + 0.5*w*prop))

            boxes_ext.append( (t, l, b, r) )

        boxes_ext = np.array(boxes_ext, dtype='int32')
        
        return boxes_ext
    

    def close_session(self):
        self.sess.close()


def draw_boxes(image, boxes, labels=None, probs=None, do_copy=False, px_thickness=1, color=(36,255,12)):

    if image.dtype == np.float:
        color = [c/255.0 for c in color]
        
    if do_copy:
        image = np.copy(image)
        
    for i_box, (t, l, b, r) in enumerate(boxes):
##        print('Box', i_box , ':', (t, l, b, r))
        image = cv2.rectangle(image, (l, t), (r, b), color, px_thickness)

        text = ''
        if labels is not None:
            text += str( labels[i_box] )

        if probs is not None:
            text += '[{:0.02f}]'.format( probs[i_box] )

        if text != '':
            t_text = t-10 if t - 10 > 10 else t + 10
            cv2.putText(image, text, (l, t_text), cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, px_thickness, cv2.LINE_AA)
        
    return image



def letterbox_image_cv2(image, size=None, shape=None, color=(128, 128, 128)):
    '''Resize image with unchanged aspect ratio using padding'''

    if image.dtype == np.float:
        color = [c/255.0 for c in color]
    
    img_height, img_width = image.shape[:2]

    if size is None and shape is None:
        raise Exception(' - letterbox_image_cv2: no both arguments can not be None (size, shape)')

    if size is not None:
        shape = size[::-1]
    else:
        size = shape[::-1]

    w, h = size
    
    scale = min(w / img_width, h / img_height)
    nw    = min(w, int(img_width  * scale))
    nh    = min(h, int(img_height * scale))

    image = cv2.resize(image, (nw, nh), cv2.INTER_CUBIC)

    
    top    = (h - nh) // 2
    bottom =  h - top - nh
    left   = (w - nw) // 2
    right  = w - left - nw

    new_image = cv2.copyMakeBorder(image, top, bottom, left, right, cv2.BORDER_CONSTANT, None, color)

    assert new_image.shape[:2] == shape, ' - ERROR, letterbox_image_cv2: something goes wrong.'
    return new_image




def extract_face(image, box):
    (t, l, b, r) = box
    face = image[t:b, l:r]
    return face


def extract_faces(image, boxes):
    faces_v = []

    for box in boxes:
        face = extract_face(image, box)
        faces_v.append(face)

    return faces_v


##def gender_detection(image, boxes):
##    gender_v = []
##    prob_v = []
##    for f in extract_faces(image, boxes):
##        f = np.copy( f[...,::-1] )
##        l_v, p_v = cvlib.detect_gender(f)
##
##        i = np.argmax(p_v)
##        gender_v.append( l_v[i] )
##        prob_v.append(p_v[i])
##        
##    return np.array(gender_v), np.array(prob_v)


if __name__ == '__main__':
    from PIL import Image
    import numpy as np
    from matplotlib import pyplot as plt

##    import tensorflow as tf
##
##    config = tf.ConfigProto()
##    config.gpu_options.allow_growth = True
##    sess = tf.Session(config=config)

    #import cvlib
    
    
    args = YOLO_args(img_size=[320,320],
                     score=0.5,
                     iou=0.45,
                     max_boxes=20,
                     extend_boxes_prop=1.0,
                     verbose=False)
    
    yolo = YOLO(args)

    if 1:
        img = np.array( Image.open('../samples/people1.jpg') )
        yolo.extend_boxes_prop = 1.2
        boxes, scores = yolo.detect_image(img)
##        boxes, scores = cvlib.detect_face(img); boxes = np.array([(t, l, b, r) for (l, t, r, b) in boxes], dtype=np.int)
        
##        gender_v, prob_v = gender_detection(img, boxes)
    
        img_det = draw_boxes(img, boxes, labels=None, probs=scores, do_copy=True)

        plt.imshow(img_det)
        plt.show()
            


    if 0:
        cap = cv2.VideoCapture('../samples/marcha.mkv')
        do_pause = False
        while True:
            if not do_pause:
                ret, frame = cap.read()
                if not ret:
                    break
                
                img = cv2.cvtColor(frame,cv2.COLOR_BGR2RGB)
                boxes, scores = yolo.detect_image(img)
                img_det = draw_boxes(frame, boxes, probs=scores)

##            frame = cv2.cvtColor(img_det, cv2.COLOR_RGB2BGR)
            
            cv2.imshow('frame',frame)
            key = cv2.waitKey(1) & 0xFF
            if key == ord('q'):
                break

            elif key == ord('p'):
                do_pause = not do_pause

            
        print('EoV')
##        yolo.close_session()
        cap.release()
        cv2.destroyAllWindows()
